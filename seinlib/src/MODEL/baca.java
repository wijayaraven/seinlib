/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODEL;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author x.x
 */
public class baca {
      //getter dan seter semua var 
    private byte[] buku;
    private String judul;
    private String penulis;
    private String genre;
    private String status;
    private String rate;
    private int id;
 private byte[] image;

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

   
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

   
    public byte[] getBuku() {
        return buku;
    }

    public void setBuku(byte[] buku) {
        this.buku = buku;
    }

  

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getPenulis() {
        return penulis;
    }

    public void setPenulis(String penulis) {
        this.penulis = penulis;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

  


  

 
}
