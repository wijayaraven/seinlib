/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MODEL;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author x.x
 */
public class toko {

    //getter dan seter semua var dibawah
    //untuk mengambil dan menset
  
    private String namatoko;
    private String lokasi;
    private String judulbuku;
    private int jumlahbuku;
    
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamatoko() {
        return namatoko;
    }

    public void setNamatoko(String namatoko) {
        this.namatoko = namatoko;
    }

    public String getLokasi() {
        return lokasi;
    }

    public void setLokasi(String lokasi) {
        this.lokasi = lokasi;
    }

    public String getJudulbuku() {
        return judulbuku;
    }

    public void setJudulbuku(String judulbuku) {
        this.judulbuku = judulbuku;
    }

    public int getJumlahbuku() {
        return jumlahbuku;
    }

    public void setJumlahbuku(int jumlahbuku) {
        this.jumlahbuku = jumlahbuku;
    }

    


}
