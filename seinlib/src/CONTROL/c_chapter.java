/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import MODEL.chapter;
import KONEKSI.koneksi;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class c_chapter {
    private Connection conn = null;

    public c_chapter(Connection con) {
        this.conn = con;
    } public List<chapter> getchapter() {
        List<chapter> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareCall("SELECT * FROM chapter");
            rs = ps.executeQuery();
            while (rs.next()) {
                chapter chap= new chapter();
               chap.setId(rs.getInt(5));
                chap.setJudul(rs.getString(4));
                chap.setChapter(rs.getString(1));
               chap.setPage(rs.getString(3));
                chap.setImage(rs.getBytes(2));
          
                get.add(chap);
            }
        } catch (Exception e) {
        }
        return get;
    }
    public List<chapter> carichaps(String judul,String chapter,String page) {
       List<chapter> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareStatement("SELECT * FROM chapter WHERE judul ='"+judul+"' AND chapter ='"+chapter+"' AND page='"+page+"'");
            rs = ps.executeQuery();
            while (rs.next()) {
            chapter chap= new chapter();
               
              
                chap.setJudul(rs.getString(4));
                chap.setChapter(rs.getString(1));
                chap.setPage(rs.getString(3));
                chap.setImage(rs.getBytes(2));
          
                get.add(chap);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return get;
    }
    
  public List<chapter> carichapter(String nama) {
       List<chapter> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareStatement("SELECT * FROM chapter WHERE judul ='"+nama+"'");
            rs = ps.executeQuery();
            while (rs.next()) {
            chapter chap= new chapter();
               
              
                chap.setJudul(rs.getString(4));
                chap.setChapter(rs.getString(1));
                chap.setPage(rs.getString(3));
                chap.setImage(rs.getBytes(2));
          
                get.add(chap);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return get;
    }
  
  
    
     public boolean inputbuku(chapter chap) {
        boolean vali = false;
        PreparedStatement ps;
      
        try {
            
            ps = conn.prepareStatement("INSERT INTO `chapter`(`judul`, `chapter`, `page`,`image`) " + "VALUES(?, ?, ?, ?)");
            ps.setString(1, chap.getJudul());
            ps.setString(2, chap.getChapter());
            ps.setString(3, chap.getPage());
            ps.setBytes(4, chap.getImage());
            
            
        
          
          
            if (ps.executeUpdate() > 0) {
                vali = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vali;
    
  }
       public String hapus(String id) {
        String hasil = "tidak berhasil hapus";//menghapus data di tb_buku dimana idbuku yang ada di tb_buku= idbuku yang diinput
        String query = "DELETE FROM chapter WHERE id='" + id + "'";
        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(query) > 0) {
                hasil = "berhasil hapus";
            }
        } catch (SQLException ex) {
              ex.printStackTrace();
        }
        return hasil;
    }
       public String Updatebuku(String judul, String chapter, String page,int id) {
        String hasil = "tidak berhasil update";//mengupdate tb_buku sesuai dengan atribut diatas
        String sql =  "UPDATE `chapter` SET `judul`='" + judul +"', `chapter`='"+ chapter +"', `page`='"+ page+ "'WHERE  `id`=" +id;

        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(sql) > 0) {
                hasil = "berhasil update";
            }
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
        return hasil;//mengembalikan 
    }


  
  
     }
  
 

 
