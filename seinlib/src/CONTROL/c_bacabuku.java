/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import MODEL.baca;
import KONEKSI.koneksi;
import com.mysql.jdbc.Connection;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class c_bacabuku {
    private Connection conn = null;
String s;
    public c_bacabuku(Connection con) {
        this.conn = con;
    } public List<baca> getbacabuku() {
        List<baca> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareCall("SELECT * FROM baca");
            rs = ps.executeQuery();
            while (rs.next()) {
              baca buk= new baca();
               
                buk.setId(rs.getInt(1));
                buk.setJudul(rs.getString(2));
                buk.setPenulis(rs.getString(3));
                buk.setGenre(rs.getString(4));
                buk.setStatus(rs.getString(5));
             buk.setRate(rs.getString(6));
              buk.setBuku(rs.getBytes(7));
                get.add(buk);
            }
        } catch (Exception e) {
        }
        return get;
    }

    
     public List<baca> cari(String nama) {
         List<baca> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareStatement("SELECT * FROM baca WHERE judul ='"+nama+"'");
            rs = ps.executeQuery();
            while (rs.next()) {
               baca buk= new baca();
               buk.setId(rs.getInt(1));
                buk.setJudul(rs.getString(2));
                buk.setPenulis(rs.getString(3));
                buk.setGenre(rs.getString(4));
                buk.setStatus(rs.getString(5));
                buk.setRate(rs.getString(6));
                buk.setBuku(rs.getBytes(7));
                get.add(buk);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return get;
    }
   
    
    
     public boolean inputbuku(baca bac) {
        boolean vali = false;
        PreparedStatement ps;
      
        try {
            
            ps = conn.prepareStatement("INSERT INTO `baca`(`judul`, `penulis`, `genre`,`status`, `rate`, `buku`) " + "VALUES(?, ?, ?, ?,?,?)");
            ps.setString(1, bac.getJudul());
            ps.setString(2, bac.getPenulis());
            ps.setString(3, bac.getGenre());
            ps.setString(4, bac.getStatus());
            ps.setString(5, bac.getRate());
            ps.setBytes(6, bac.getImage());
            
            
        
          
          
            if (ps.executeUpdate() > 0) {
                vali = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vali;
    
  }
       public String hapus(String id) {
        String hasil = "tidak berhasil hapus";//menghapus data di tb_buku dimana idbuku yang ada di tb_buku= idbuku yang diinput
        String query = "DELETE FROM baca WHERE id='" + id + "'";
        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(query) > 0) {
                hasil = "berhasil hapus";
            }
        } catch (SQLException ex) {
              ex.printStackTrace();
        }
        return hasil;
    }
       public String Updatebuku(String judul, String penulis, String genre,String status,String rate,int id) {
        String hasil = "tidak berhasil update";//mengupdate tb_buku sesuai dengan atribut diatas
        String sql =  "UPDATE `baca` SET `judul`='" + judul +"', `penulis`='"+ penulis +"', `genre`='"+ genre+ "', `status`='"+status+"', `rate`='"+rate +"' WHERE  `id`=" +id;

        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(sql) > 0) {
                hasil = "berhasil update";
            }
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
        return hasil;//mengembalikan 
    }

  
  
     }
  
 

 
