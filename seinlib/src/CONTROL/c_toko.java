/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import MODEL.toko;
import KONEKSI.koneksi;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class c_toko {
    private Connection conn = null;

    public c_toko(Connection con) {
        this.conn = con;
    } public List<toko> gettoko() {
        List<toko> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareCall("SELECT * FROM tb_toko");
            rs = ps.executeQuery();
            while (rs.next()) {
              toko tok= new toko();
                tok.setNamatoko(rs.getString(1));
                tok.setLokasi(rs.getString(2));
                tok.setJudulbuku(rs.getString(3));
                tok.setJumlahbuku(rs.getInt(4));
                tok.setId(rs.getInt(5));
                get.add(tok);
            }
        } catch (Exception e) {
        }
        return get;
    }

    
     public List<toko> cari(String nama) {
        List<toko> get = new ArrayList<>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareStatement("SELECT * FROM tb_toko WHERE judul LIKE '%"+nama+"%'");
            rs = ps.executeQuery();
            while (rs.next()) {
               toko tok= new toko();
                tok.setNamatoko(rs.getString(1));
                tok.setLokasi(rs.getString(2));
                tok.setJudulbuku(rs.getString(3));
                tok.setJumlahbuku(rs.getInt(4));
                 tok.setId(rs.getInt(5));
                get.add(tok);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return get;
    }
       public boolean inputtoko(toko tok) {
        boolean vali = false;
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement("INSERT INTO `tb_toko`(`nama toko`, `lokasi`, `judul`, `jumlah buku`) " + "VALUES(?, ?, ?, ?)");
            ps.setString(1, tok.getNamatoko());
            ps.setString(2, tok.getLokasi());
            ps.setString(3, tok.getJudulbuku());
            ps.setInt(4, tok.getJumlahbuku());
            
        
          
          
            if (ps.executeUpdate() > 0) {
                vali = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vali;
    
  }
       
        
       public String hapus(String id) {
        String hasil = "tidak berhasil hapus";//menghapus data di tb_buku dimana idbuku yang ada di tb_buku= idbuku yang diinput
        String query = "DELETE FROM tb_toko WHERE id='" + id + "'";
        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(query) > 0) {
                hasil = "berhasil hapus";
            }
        } catch (SQLException ex) {
              ex.printStackTrace();
        }
        return hasil;
    }
       public String Updatebuku(String namatoko, String lokasi, String judul,int jum,int id) {
        String hasil = "tidak berhasil update";//mengupdate tb_buku sesuai dengan atribut diatas
        String sql =  "UPDATE `tb_toko` SET `nama toko`='" + namatoko +"', `lokasi`='"+ lokasi +"', `judul`='"+ judul+ "', `jumlah buku`='"+jum+"' WHERE  `id`=" +id;

        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(sql) > 0) {
                hasil = "berhasil update";
            }
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
        return hasil;//mengembalikan 
    }

  
  
     }
  
 

 
