/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import MODEL.buku;
import KONEKSI.koneksi;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
public class c_buku {
    private Connection conn = null;

    public c_buku(Connection con) {
        this.conn = con;
    } public List<buku> getbuku() {
        List<buku> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareCall("SELECT * FROM tb_buku");
            rs = ps.executeQuery();
            while (rs.next()) {
              buku buk= new buku();
                buk.setIdbuku(rs.getInt(1));
                buk.setJudul(rs.getString(2));
                buk.setPenulis(rs.getString(3));
                buk.setPerpus(rs.getString(4));
                buk.setAlamat(rs.getString(5));
                buk.setJumlah(rs.getInt(6));
                get.add(buk);
            }
        } catch (Exception e) {
        }
        return get;
    }

    
     public List<buku> cari(String nama) {
        List<buku> get = new ArrayList<>();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareStatement("SELECT * FROM tb_buku WHERE judul LIKE '%"+nama+"%'");
            rs = ps.executeQuery();
            while (rs.next()) {
                buku buk= new buku();
                buk.setIdbuku(rs.getInt(1));
                buk.setJudul(rs.getString(2));
                buk.setPenulis(rs.getString(3));
                buk.setPerpus(rs.getString(4));
                buk.setAlamat(rs.getString(5));
                buk.setJumlah(rs.getInt(6));
                get.add(buk);
            }
        } catch (SQLException ex) {
            System.out.println(ex);
        }
        return get;
    }
       public boolean inputbuku(buku buk) {
        boolean vali = false;
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement("INSERT INTO `tb_buku`(`judul`, `penulis`, `perpus`, `alamat`, `jumlah`) " + "VALUES(?, ?, ?, ?,?)");
            ps.setString(1, buk.getJudul());
            ps.setString(2, buk.getPenulis());
            ps.setString(3, buk.getPerpus());
            ps.setString(4, buk.getAlamat());
            ps.setInt(5, buk.getJumlah());
            
        
          
          
            if (ps.executeUpdate() > 0) {
                vali = true;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return vali;
    
  }
       public String hapus(String id) {
        String hasil = "tidak berhasil hapus";//menghapus data di tb_buku dimana idbuku yang ada di tb_buku= idbuku yang diinput
        String query = "DELETE FROM tb_buku WHERE idbuku='" + id + "'";
        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(query) > 0) {
                hasil = "berhasil hapus";
            }
        } catch (SQLException ex) {
              ex.printStackTrace();
        }
        return hasil;
    }
       public String Updatebuku(String judul, String penulis, String perpus,String alamat,int jum,int id) {
        String hasil = "tidak berhasil update";//mengupdate tb_buku sesuai dengan atribut diatas
        String sql =  "UPDATE `tb_buku` SET `judul`='" + judul +"', `penulis`='"+ penulis +"', `perpus`='"+ perpus+ "', `alamat`='"+alamat+"', `jumlah`='"+jum +"' WHERE  `idbuku`=" +id;

        Statement st;
        try {
            st = conn.createStatement();
            if (st.executeUpdate(sql) > 0) {
                hasil = "berhasil update";
            }
        } catch (SQLException ex) {
             ex.printStackTrace();
        }
        return hasil;//mengembalikan 
    }

  
  
     }
  
 

 
