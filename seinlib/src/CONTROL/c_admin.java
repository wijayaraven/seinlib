/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROL;

import MODEL.admin;
import KONEKSI.koneksi;
import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
public class c_admin {
    private Connection conn = null;

    public c_admin(Connection con) {
        this.conn = con;
    }
    public boolean inputadmin(admin ad) {
        boolean vali = false;
        PreparedStatement ps;
        try {
            ps = conn.prepareStatement("INSERT INTO tb_admin(username,password,nama,alamat) VALUES(?,?,?,?)");

           
            ps.setString(2, ad.getUsername());
            ps.setString(3, ad.getPassword());
            ps.setString(4, ad.getNama());
            ps.setString(5, ad.getAlamat());
           
         
          
            if (ps.executeUpdate() > 0) {
                vali = true;
            }
        } catch (Exception e) {
        }
        return vali;
  }
      
 public List<admin> getadmin() {
        List<admin> get = new ArrayList();
        PreparedStatement ps;
        ResultSet rs;
        try {
            ps = conn.prepareCall("SELECT * FROM tb_admin");
            rs = ps.executeQuery();
            while (rs.next()) {
                admin ad = new admin();
                ad.setIdadmin(rs.getInt(1));
                ad.setUsername(rs.getString(2));
                ad.setPassword(rs.getString(3));
                ad.setNama(rs.getString(4));
                ad.setAlamat(rs.getString(5));
               
                
                get.add(ad);
            }
        } catch (Exception e) {
        }
        return get;
    }
  
 
}
 
